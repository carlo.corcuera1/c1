import { Form, Button } from 'react-bootstrap';
import { useState, useEffect, useContext } from 'react';
import UserContext from '../UserContext';
import { Navigate, useNavigate } from 'react-router-dom';

import Swal from 'sweetalert2';


export default function Register() {

	const {user} = useContext(UserContext);

	// State hooks to store values of the input fields
	const [email, setEmail] = useState("");
	const [password1, setPassword1] = useState("");
	const [password2, setPassword2] = useState("");
	const [firstName, setFirstName] = useState("");
	const [lastName, setLastName] = useState("");
	const [mobileNo, setMobileNo] = useState("");

	// State to determine whether submit button is enabled or not
	const [isActive, setIsActive] = useState(false);

	const navigate = useNavigate();

	function registerUser(e){

		fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				email: email,
				mobileNo: mobileNo,
				password: password1
			})
		})
		.then(res => res.json())
		.then(data => {

			if(data) {

				Swal.fire({
					title: "Registration Successful!",
					icon: "success",
					text: "Welcome to Zuitt!"
				})

				setEmail("");
				setPassword2("");
				setPassword1("");
				setFirstName("");
				setLastName("");
				setMobileNo("");
				setIsActive(false);
				navigate("/login")
			} else {

				Swal.fire({

					title: "Something went wrong!",
					icon: "error",
					text: "Please try again!"
				})
			}
		})
	}

	const emailExist = (e) => {
		e.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/users/checkEmail`, {
			method: 'POST',
			headers: {'Content-Type': 'application/json'},
			body: JSON.stringify({email})
		})
		.then(res => res.json())
		.then(data => {

			if(data === true){

				Swal.fire({
					title: "Duplicate email found",
					icon: 'error',
					text: 'Please provide a different email!'
				})
			} else {
				registerUser()
			}
		})
	}

	useEffect(() => {

		// Validation to enable submit button when all fields are populated and both passwords match

		if((email !== "" && password1 !== "" && password2 !== "" && firstName !== "" && lastName !== "") && (password1 === password2) && (password1.length && password2.length >= 8) && (mobileNo.length >= 11)){
			setIsActive(true);
		} 

	}, [ email, password1, password2, firstName, lastName, mobileNo ]);
	
	return (
        (user.id !== null) ?
	        <Navigate to ="/"/>

        :

	        <Form onSubmit={emailExist}>
		        <Form.Group controlId="userFirstName" className="my-2">
			        <Form.Label>First Name</Form.Label>
				        <Form.Control 
					        type="text" 
					        placeholder="Enter First Name"
					        value = {firstName}
					        onChange = {e => setFirstName(e.target.value)} 
					        required
				        /> 
		        </Form.Group>

	        <Form.Group controlId="userLastName" className="my-2">
	        	<Form.Label>Last Name</Form.Label>
			        <Form.Control 
				        type="text" 
				        placeholder="Enter Last Name"
				        value = {lastName}
				        onChange = {e => setLastName(e.target.value)} 
				        required
				    /> 
	        	</Form.Group>

	        <Form.Group controlId="userEmail" className="my-2">
		        <Form.Label>Email address</Form.Label>
			        <Form.Control 
				        type="email" 
				        placeholder="Enter email"
				        value = {email}
				        onChange = {e => setEmail(e.target.value)} 
				        required
				        />
	        </Form.Group>

	        <Form.Group controlId="mobileNo" className="my-2">
		        <Form.Label>Mobile Number</Form.Label>
			        <Form.Control 
				        type="text" 
				        placeholder="Enter Mobile Number"
				        value = {mobileNo}
				        onChange = {e => setMobileNo(e.target.value)} 
				        required
			        /> 
	        </Form.Group>

	        <Form.Group controlId="password1" className="my-2">
		        <Form.Label>Password</Form.Label>
			        <Form.Control 
				        type="password" 
				        placeholder="Password"
				        value = {password1}
				        onChange = {e => setPassword1(e.target.value)}
				        required
			        />
			        <Form.Text className="text-muted">
			        	We'll never share your email with anyone else.
			        </Form.Text>
	        </Form.Group>

	        <Form.Group controlId="password2" className="my-2">
		        <Form.Label>Verify Password</Form.Label>
			        <Form.Control 
				        type="password" 
				        placeholder="Verify Password" 
				        value = {password2}
				        onChange = {e => setPassword2(e.target.value)}
				        required
			        />
			        <Form.Text className="text-muted">
			        	Must be 8 characters or more!
			        </Form.Text>
	        </Form.Group>

        { isActive ?
	        <Button variant="primary" type="submit" id="submitBtn" >
	        Submit
	        </Button>
	        :
	        <Button variant="danger" disabled type="submit" id="submitBtn">
	        Submit
	        </Button>
	    }

	</Form>
	)
}